Devuan on Synology DS213j
=========================

This project is about installing a full unrestricted linux
distribution on an Synology DS213j NAS hardware. 

Overview
--------

- Some documentation about progrss can be found in `doc`
  subfolder

- Resources related to the linux kernel are located in
  the `kernel` subfolder

State
-----

The project is ongoing, a first tftp and nfs based boot 
of Devuan linux was sucessful.

Contact
-------

On questions, support or ideas, send an email to 
andi@bastelmap.de.

