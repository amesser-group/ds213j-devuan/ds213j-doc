
First Steps
===========

The first step of installing Linux on the DS213j was to check if
it is possible to boot a standard linux at all. 

Get access to the boot loader
-----------------------------

I started with 
installing the original DSM software at first to check if the 
hardware was OK and to ensure that the most recent software 
(including bootloader) was installed on the device.

Now, using a serial cable attached to mainboard, the whole 
boot process could be watched ending in a typicall linux login 
prompt. The bootloader was "u-boot" and pressing Ctrl+C when
prompted so, the boot sequence could be interrupted. Luckily,
the loader was also able to load images through tftp. This could 
be used for first, non-intrusive tests. As a drawback the 
default boot sequence  seems to be more or less hardcoded 
since the environment variable store was not properly configured.
This for a permanent install, the new kernel and initrd need to
fit the pre-configured flash partitioning and the kernel must
be using a hard coded command line. The command line
passed by u-boot needs to be ignored since it is to set it
permanently to a proper value for the new kernel. Also the device 
tree can not be passed by u-boot.

Build a linux kernel
--------------------

Nevertheless, everything is read for initial testing now. At first
a kernel needs to be prepared. Downloading and unzipping the kernel
sources as usual and after installing an "gnueabiarmhf" cross compiler the 
kernel could be initially configured using

::

  ~$ cd linux
  ~/linux$ make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- mvebu_v7_defconfig
  ~/linux$ make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- -j3 menuconfig

In the last step I disabled several unneeded features since the kernel needs 
only to run one particular hardware. A premade config file is in 
`kernel/config-u-boot-commandline`

Aftwards building the kernel and creating .deb packages ::

  ~/linux$ make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- -j<your_cpu_count> bindeb-pkg

For a first step the u-boot loader can be used to download the kernel through
tftp. For that purpose the a tftp server needs to be installed and an u-boot image
needs to be created. The device tree needs to be appended to the kernel image
since the DS213j u-boot loader can not handle device trees ::

  ~/linux$ cat arch/arm/boot/zImage arch/arm/boot/dts/armada-370-synology-ds213j.dtb >/tmp/Image
  ~/linux$ mkimage -A arm -O linux -T kernel -C none -a 0x8000 -e 0x8000 -n "Linux " -d /tmp/Image /srv/tftp/uImage

Now connecting the DS213j to the network and powering it on, the 
following commands can be issued to u-boot to start the kernel::

  <Ctrl+C>
  dhcp
  tftp 0x02000000 <your tftpserver ip>:uImage
  set bootargs debug console=ttyS0,115200 root=/dev/ram0
  bootm 0x02000000

This boots the new kernel and ends with an kernel panic due to missing 
init. Everything fine so far.

Prepare a linux
---------------

The following instructions are for a Devuan Beowulf Linux distribution, But
other Distributions should be working also. The Idea is to prepare an armhf
Linux distribution root filesystem on the workstation and export it through
NFS for use by the Synology NAS. This is non-intrusive and will work without
making any permanent changes to the DS213j. 

.. note::
  
  Officially the reading is, that the armhf port of Debian will not work
  on ARM cpus without NEON instruction set. (like the DS213j) But for me
  I did not encounter any issues yet. The armle port can be used as well,
  but since armhf makes use of the FPU the performance might be better
  in certain usecases.

Since the "armhf" will be in most cases not matching the workstations 
architecture, a static qemu and kernel binfmt support needs to be 
installed on the workstation::

  $ apt install qemu-user-static qemu-user-static binfmt-support

On Debian based systems it is easy to bootstrap a new linux installation using
the `debootstrap` tool. Using the tool will require root permissions. Also for 
me I dont want to have the new root filesystem in users home directory. I decided
to put it under /var::

  $ sudo mkdir /var/os/devuan-beowulf-armhf
  $ sudo debootstrap --arch=armhf --foreign beowulf /var/os/devuan-beowulf-armhf
  $ sudo cp /usr/bin/qemu-arm-static /var/os/devuan-beowulf-armhf/usr/bin/
  $ sudo dd of=/var/os/devuan-beowulf-armhf/usr/sbin/policy-rc.d<<"EOD"
  #/bin/sh

  HOSTNAME=`hostname`
  HOSTNAME_ETC=`cat /etc/hostname`

  if [ "x${HOSTNAME}" != "x${HOSTNAME_ETC}" ]; then
    # prevent starting services on workstation
    # probably works for insserv only
    exit 101
  fi

  exit 0
  EOD
  $ sudo chmod +x /var/os/devuan-beowulf-armhf/usr/sbin/policy-rc.d
  $ sudo chroot /var/os/devuan-beowulf-armhf/debootstrap/debootstrap --second-stage

Finally in order to make the `policy-rc.d` script work as expected, the new installation
needs to get a different hostname by editting `/var/os/devuan-beowulf-armhf/etc/hostname`.

Now the new file system needs to be populated with some sensible packets in order
to become usefull::

  $ sudo cp *.deb /var/os/devuan-beowulf-armhf/root/ # copy linux kernel debs buit above
  $ sudo chroot /var/os/devuan-beowulf-armhf /bin/bash
  $ apt install initramfs-tools lvm2 mdadm busybox openssh-server vim
  $ dpkg -i /root/linux-*_armhf.deb
  $ passwd # set a root password here
  $ exit # leave chroot

Finally, the file `/var/os/devuan-beowulf-armhf/etc/inittab` needs to be edited to enable
the gett on the serial console ttyS0. (Just uncomment the line)

Boot the NAS using nfs root
---------------------------

Having installed the kernel package an initrd should have been created also. This initrd
needs to be prepared for use by u-boot loader::

  $ mkimage -A arm -O linux -T kernel -C none -a 0x0000 -e 0x0000 -n "Initrd" -d /var/os/devuan-beowulf-armhf/boot/initrd.img-* /srv/tftp/uInitrd

The newly created filesystem needs to be exported as nfsv3 filesystem on the workstation::

  $ sudo exportfs -o rw,sync,no_subtree_check,no_root_squash <subnet of nas>:/var/os/devuan-beowulf-armhf

No everything is ready to try booting the nas through network using a nfs 
root. Going back to the serial console and powercycling the NAS, the following
u-boot commands are needed::

  <Ctrl+C>
  dhcp
  tftp 0x02000000 <your tftpserver ip>:uImage
  tftp 0x03000000 <your tftpserver ip>:uInitrd
  set bootargs debug console=ttyS0,115200 initrd=0x03000040,8M root=/dev/ram0 boot=nfs nfsroot=<ip of worksation>:/var/os/devuan-bewulf-armhf 
  bootm 0x02000000

It will take quite a while, but finally a login prompt should appear


Neverpossible to change
it to a proper value

